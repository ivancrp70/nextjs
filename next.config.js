/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

module.exports = {
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  env:{

     REACT_APP_SERVICE_ID:process.env.EACT_APP_SERVICE_ID,
     REACT_APP_TEMPLATE_ID:process.env.REACT_APP_TEMPLATE_ID,
     REACT_APP_USER_ID:process.env.REACT_APP_USER_ID,

// GOOGLE	
      REACT_APP_SITE_KEY:process.env.REACT_APP_SITE_KEY
}
}