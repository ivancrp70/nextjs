import type { Course } from '@/interfaces/course'

export const data: Array<Course> = [
  {
    id: 1,
    cover: '/images/courses/manicure.jpeg',
    title: 'Manicure',
    price: '-',
    category: 'Beleza',
  },
  {
    id: 2,
    cover: '/images/courses/pedicure.jpeg',
    title: 'Pedicure',
    price: '-',
    category: 'Beleza',
  },
  {
    id: 3,
    cover: '/images/courses/posticas_realista.jpeg',
    title: 'Postiça realista',
    price: '-',
    category: 'Beleza',
  },
  {
    id: 4,
    cover: '/images/courses/posticas_simples.jpeg',
    title: 'Postiça simples',
    price: '-',
    category: 'Beleza',
  },

  {
    id: 5,
    cover: '/images/courses/blindagem_gel.jpeg',
    title: 'Blindagem de gel',
    price: '-',
    category: 'Beleza',
  },
  {
    id: 6,
    cover: '/images/courses/blindagem.jpeg',
    title: 'Blindagem',
    price: '-',
    category: 'Beleza',
  },
  {
    id: 7,
    cover: '/images/courses/esmaltacao_gel.jpeg',
    title: 'Esmaltação em gel',
    price: '-',
    category: 'Beleza',
  },
]
