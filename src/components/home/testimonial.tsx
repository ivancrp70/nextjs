import React, { FC, useRef } from 'react'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'

const HomeTestimonial: FC = () => {

  return (
    <Box id="testimonial" sx={{ backgroundColor: 'background.paper', py: { xs: 8, md: 10 } }}>
      <Container>

        <Typography variant="h1" sx={{ color: '#F1A7BC', mt: { xs: 0, md: -5 }, fontSize: { xs: 30, md: 48 } }}>
          Localização
        </Typography>
        <Box
          sx={{

            textAlign: 'center',
          }}
        >
          <iframe width="100%" height="400px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3980.796965924815!2d-38.50254501555643!3d-3.853708033801085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c751dd43182a53%3A0x1a05d1c785280dc!2sJC%20ATELI%C3%8A%20DE%20BELEZA!5e0!3m2!1spt-BR!2sbr!4v1686695054169!5m2!1spt-BR!2sbr">
          </iframe>
        </Box>
      </Container>
    </Box>
  )
}

export default HomeTestimonial
