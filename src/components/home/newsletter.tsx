import React, { FC, useState } from 'react'
import Box from '@mui/material/Box'
import InputBase from '@mui/material/InputBase'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button';
import { Input, TextField } from '@mui/material'
import TextareaAutosize from '@mui/base/TextareaAutosize'
import emailjs from '@emailjs/browser';
import ReCAPTCHA from "react-google-recaptcha"
import { useForm } from 'react-hook-form';

const HomeNewsLetter: FC = () => {

  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [mensagem, setMensagem] = useState('');
  const [captcha, setCaptcha] = useState(false);

  const onChange = () => {
    setCaptcha(true);
  };

  const onExpired = () => {
    setCaptcha(false);
  };

  function enviar() {
    emailjs.send("service_bfi8b3w", "template_7y1yqf8", {
      assunto: "SITE JC ATELIÊ BELEZA",
      nome: nome,
      enviado_por_name: email,
      mensagem: mensagem,
    }, "YGgZm5ccOvxsycgmJ"
    );

    alert('E-MAIL ENVIADO'),
      console.log(process.env.REACT_APP_SERVICE_ID),
      setEmail(''),
      setNome(''),
      setMensagem('')

  }

  return (
    <Box id="newsletter" sx={{
      pt: {
        xs: 6,
        md: 8,
      },
      pb: 14,
      backgroundColor: 'background.default',
    }}>
      <Container>
        <Box
          sx={{
            backgroundColor: '#F1A7BC',
            borderRadius: 10,
            py: { xs: 4, md: 10 },
            px: { xs: 4, md: 8 },
            textAlign: 'center',
          }}
        >
          <Typography variant="h1" component="h2" sx={{ color: '#FFFFFF', mb: 1, fontSize: { xs: 32, md: 42 } }}>
            Deixe seu comentário aqui!
          </Typography>
          <Box
            sx={{

              display: 'flex',
              alignItems: 'center',
              flexDirection: { xs: 'column', md: 'row' },
              justifyContent: 'space-around',
              width: { xs: '100%', md: 560 },
              mx: 'auto',
              mb: 1,
            }}
          >
            <Input required
              type='text'
              id='nome'
              sx={{
                backgroundColor: 'background.paper',
                borderRadius: 3,
                width: '100%',
                height: 48,
                px: 2,
                mr: { xs: 0, md: 3 },
                mb: { xs: 2, md: 0 },
              }}
              placeholder="Digite seu Nome"
              onChange={e => setNome(e.target.value)}
              value={nome}
            />

          </Box>
          <Box sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: { xs: 'column', md: 'row' },
            justifyContent: 'space-around',
            width: { xs: '100%', md: 560 },
            mx: 'auto',
            mb: 1,
          }}>
            <Input
              type='email'
              required
              id='email'
              sx={{
                border: 0,
                backgroundColor: 'background.paper',
                borderRadius: 3,
                width: '100%',
                height: 48,
                px: 2,
                mr: { xs: 0, md: 3 },
                mb: { xs: 2, md: 0 },
              }}
              placeholder="Digiter seu Email"
              onChange={e => setEmail(e.target.value)}
              value={email}
            />

          </Box>

          <Box sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: { xs: 'column', md: 'row' },
            justifyContent: 'space-around',
            width: { xs: '100%', md: 560 },
            mx: 'auto',
            mb: 1,
          }}>

            <TextareaAutosize
              required
              minRows={10}
              id='mensagem'
              style={{
                width: '97%',
                backgroundColor: 'background.paper',
                borderRadius: 19,
                alignItems: 'center',
                border: 1,
                marginRight: 20,
                flexDirection: 'row',
                padding: 14,
                font: 'inherit'
              }}

              placeholder="Digiter sua Mensagem"
              onChange={e => setMensagem(e.target.value)}
              value={mensagem}
            />
          </Box>

          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: { xs: 'column', md: 'row' },
              justifyContent: 'space-around',
              width: { xs: '100%', md: 560 },
              mx: 'auto',
              mb: 1,
            }}>

            <ReCAPTCHA
              sitekey="6LdwbbomAAAAAL_kf-WZBhi3h1NFRwTB3A-KoonM"
              onChange={onChange}
              onExpired={onExpired}
            />

          </Box>

          <Box >
            <Button
              disabled={!captcha || !nome || !email || !mensagem}
              variant="contained"
              size="large"
              onClick={enviar}
            >
              Enviar
            </Button>
          </Box>
        </Box>
      </Container>
    </Box>
  )
}


export default HomeNewsLetter
