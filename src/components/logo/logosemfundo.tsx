import React, { FC } from 'react'
import { Box, Typography } from '@mui/material'
import Image from 'next/image'

interface Props {
  onClick?: () => void
  variant?: 'primary' | 'secondary'
}

const LogoSemFundo: FC<Props> = ({ onClick }) => {
  return (
    <Box onClick={onClick}>

      <Image src="/images/logo-sem-fundo.png" width={220} height={220} quality={100} alt="logo sem fundo img" />

    </Box>
  )
}


LogoSemFundo.defaultProps = {
  variant: 'primary',
}

export default LogoSemFundo