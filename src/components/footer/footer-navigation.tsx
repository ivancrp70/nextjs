import React, { FC } from 'react'
import Link from 'next/link'
import Grid from '@mui/material/Grid'
import MuiLink from '@mui/material/Link'
//import type { Navigation } from '@/interfaces/navigation'
import { FooterSectionTitle } from '@/components/footer'
import { Link as ScrollLink } from 'react-scroll'
import { Box } from '@mui/material'
import { navigations } from '@/components/navigation/navigation.data'


const courseMenu = [
  {
    label: 'Manicure e Pedicure',
    path: 'popular-course',
  },
  {
    label: 'Postiça Realista e Simples',
    path: 'popular-course',
  },
  {
    label: 'Blindagem',
    path: 'popular-course',
  },
  {
    label: 'Esmaltação',
    path: 'popular-course',
  },
]
interface NavigationItemProps {
  label: string
  path: string
}


const NavigationItem: FC<NavigationItemProps> = ({ label, path }) => {
  return (
    <Link href={path} passHref>
      <MuiLink
        underline="hover"
        sx={{
          display: 'block',
          mb: 1,
          color: 'primary.contrastText',
        }}
      >
        {label}
      </MuiLink>
    </Link>
  )
}

const FooterNavigation: FC = () => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={4}>
        <FooterSectionTitle title="Serviços" />
        {courseMenu.map(({ label, path }, index) => (
          <NavigationItem key={index + path} label={label} path={/* path */ ''} />
        ))}
      </Grid>
      <Grid item xs={12} md={4}>
        <FooterSectionTitle title="Menu" />
        {navigations.map(({ path: destination, label }) => (
          <Box
            component={ScrollLink}
            key={destination}
            activeClass="current"
            to={destination}
            spy={true}
            smooth={true}
            duration={350}
            sx={{
              display: 'flex',
              underline: 'hover',
              cursor: 'pointer',
              textDecoration: 'none',
              mb: 1,
              underLine: ':hover',



            }}
          >
            <MuiLink
              underline="hover"
              sx={{
                display: 'block',
                mb: 1,
                color: 'primary.contrastText',
              }}
            >
            </MuiLink>
            {label}
          </Box>
        ))}
      </Grid>
      {/* <Grid item xs={12} md={4}>
        <FooterSectionTitle title="About" />
        {companyMenu.map(({ label, path }, index) => (
          <NavigationItem key={index + path} label={label} path={path} />
        ))}
      </Grid> */}
    </Grid>
  )
}

export default FooterNavigation
