import type { Navigation } from '@/interfaces/navigation'

export const navigations: Navigation[] = [
  {
    label: 'Home',
    path: 'hero', // '/',
  },
  {
    label: 'Serviços',
    path: 'popular-course', // '/popular-course',
  },
  {
    label: 'Localização',
    path: 'testimonial', // '/testimonial',
  },
  {
    label: 'Fale Conosco',
    path: 'newsletter', // '/mentors',
  },
]
