export interface Course {
  id: number | string
  title: string
  cover: string
 // price: number
  price: string
  category: string
}
